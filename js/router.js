export default class Router {
  constructor(routes) {
    this.routes = routes;
    this.rootElem = document.getElementById("app");

    this.init();
  }

  init() {
    this.listenRouteChange();
    this.handleRouteChange();
  }

  listenRouteChange() {
    window.addEventListener("hashchange", () => {
      this.handleRouteChange();
    });
  }

  handleRouteChange() {
    if (window.location.hash.length > 0) {
      this.routes.forEach(async (route) => {
        if (route.isActiveRoute(window.location.hash.substring(1))) {
          await this.goToRoute(route.htmlName);
          route.hasChanged();
        }
      });
    } else {
      this.routes.forEach(async (route) => {
        if (route.default) {
          await this.goToRoute(route.htmlName);
          route.hasChanged();
        }
      });
    }
  }

  async goToRoute(htmlName) {
    const url = "views/" + htmlName;
    const response = await fetch(url);
    if (response.status === 200) {
      const text = await response.text();
      this.rootElem.innerHTML = text;
    }
  }
}
