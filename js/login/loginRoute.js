import DataService from "../dataService.js";
import FormValidator from "../formValidator.js";
import Route from "../route.js";
import { validationRules } from "./loginFormValidator.js";

export default class LoginRoute extends Route {
  constructor() {
    super("login", "login.html", true);
    this.registrationValidator = new FormValidator(validationRules);
  }

  hasChanged() {
    const $loginForm = document.getElementById("login-form");

    $loginForm.addEventListener("submit", (event) => {
      this.onLoginSubmit(event);
    });
  }

  onLoginSubmit(event) {
    event.preventDefault();
    const $form = event.target;

    let isFormValid = this.registrationValidator.validateForm($form);

    if (isFormValid) {
      const $emailInput = $form.querySelector("#email");
      const $passwordInput = $form.querySelector("#password");

      const email = $emailInput.value;
      const password = $passwordInput.value;

      const user = DataService.getUser(email);

      if (user) {
        if (user.password === password) {
          alert("Succsess!");
        } else {
          alert("Auth failed!");
        }
      } else {
        alert("User is not registered.");
      }

      $emailInput.value = "";
      $passwordInput.value = "";
    }
  }
}
