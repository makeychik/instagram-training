import { isEmpty, minLength } from "../validators.js";

export const validationRules = {
  email: [
    {
      validator: isEmpty,
      errorMessage: "Enter your email.",
    },
  ],
  password: [
    {
      validator: isEmpty,
      errorMessage: "Enter a password.",
    },
    {
      validator: minLength(6),
      errorMessage: "Password must be at least 6 characters.",
    },
  ],
};
