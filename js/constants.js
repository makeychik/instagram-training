export const REGISTRATION_FORM_ID = "registration-form";
export const LOGIN_FORM_ID = "login-form";

export const LOGIN_ROUTE_HTML = "login.html";
export const REGISTRATION_ROUTE_HTML = "register.html";

export const FORM_INPUT_INVALID_CLASS = "auth-form__input_invalid";
