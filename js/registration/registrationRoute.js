import { REGISTRATION_FORM_ID } from "../constants.js";
import DataService from "../dataService.js";
import FormValidator from "../formValidator.js";
import Route from "../route.js";
import { validationRules } from "./registrationFormValidator.js";

export default class RegistrationRoute extends Route {
  constructor() {
    super("register", "register.html");
    
  }

  hasChanged() {
    const $registrationForm = document.getElementById(REGISTRATION_FORM_ID); // this

    this.registrationValidator = new FormValidator(validationRules, REGISTRATION_FORM_ID);

    $registrationForm.addEventListener("submit", (event) => {
      this.onRegistrationSubmit(event);
    });
  }

  onRegistrationSubmit(event) {
    event.preventDefault();
    const $form = event.target;

    let isFormValid = this.registrationValidator.validateForm($form);

    if (isFormValid) {
      const $inputs = $form.querySelectorAll(".auth-form__input");
      const newUser = {};

      $inputs.forEach(({ value, id }) => (newUser[id] = value));

      DataService.saveUser(newUser);

      // router.goToRoute(LOGIN_ROUTE_HTML); // ? как перейти если роутер в апп
      window.location.hash = "#login"; // костылёк, наверное
    }
  }
}
