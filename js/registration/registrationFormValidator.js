import {
  hasAtSymbol,
  isEmail,
  isEmpty,
  isPassword,
  maxLength,
  minLength,
} from "../validators.js";

export const validationRules = {
  email: [
    {
      validator: isEmail,
      errorMessage: "Email is not valid.",
    },
    {
      validator: isEmpty,
      errorMessage: "Enter your email.",
    },
    {
      validator: hasAtSymbol,
      errorMessage: "Email must have @.",
    },
    {
      validator: minLength(12),
      errorMessage: "Email must have at least 12 characters.",
    },
    {
      validator: maxLength(50),
      errorMessage: "Email can't contain more than 50 characters.",
    },
  ],
  username: [
    {
      validator: isEmpty,
      errorMessage: "Enter an username.",
    },
  ],
  password: [
    {
      validator: isEmpty,
      errorMessage: "Enter a password.",
    },
    {
      validator: isPassword,
      errorMessage: "Password is not valid.",
    },
  ],
  age: [{}],
};

export function validate(regex, value) {
  return regex.test(value);
}
