export const isEmpty = (value) => (value.length ? true : false);
export const isEmail = (value) => /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+.[a-zA-Z]{2,4}$/.test(value);
export const isPassword = (value) => /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/.test(value);
export const hasAtSymbol = (value) => /@/.test(value)
export const isValidBirthday = (value) => {

}

export const isValigAge = (value) => {

}

export const isConfirmedPasswordMatch = (value) => {

}

export const minLength = (requiredLength) => {
  return function (value) {
    return value.length >= requiredLength;
  }
}

export const maxLength = (requiredLength) => {
  return function (value) {
    return value.length <= requiredLength;
  }
}