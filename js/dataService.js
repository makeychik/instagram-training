export default class DataService {
  static saveUser(user) {
    const users = JSON.parse(localStorage.getItem("users")) || [];
    users.push(user);

    localStorage.setItem("users", JSON.stringify(users));
  }

  static getUser(userEmail) {
    const users = JSON.parse(localStorage.getItem("users"));
    const user = users.find((el) => el.email === userEmail);

    return user;
  }
}
