export default class Route {
  constructor(name, htmlName, isDefaultRoute) {
    this.name = name;
    this.htmlName = htmlName;
    this.default = isDefaultRoute;
  }

  isActiveRoute(hashedPath) {
    return hashedPath.replace("#", "") === this.name;
  }

  hasChanged() {

  }
}
