import { FORM_INPUT_INVALID_CLASS } from "./constants.js";

export default class FormValidator {
  constructor(validationRules, ID) {
    this.validationRules = validationRules;
    this.$form = document.getElementById(ID);
  }

  validateForm() {
    let isFormValid = true;

    const $inputs = this.$form.querySelectorAll(".input"); // rename maybe

    $inputs.forEach(($input) => {
      let isInputValid = this.validateInput($input);
      if (!isInputValid) {
        isFormValid = false;
      }
    });

    return isFormValid;
  }

  validateInput($input) {
    let isInputValid = true;

    this.removeInputErrorClass($input);
    this.removeErrorMessage($input);

    if (this.validationRules[$input.id]) {
      this.validationRules[$input.id].forEach(({ validator, errorMessage }) => {
        if (!validator($input.value)) {
          isInputValid = false;
          this.setInputErrorClass($input);
          this.showErrorMessage($input, errorMessage);
        }
      });
    }

    return isInputValid;
  }

  setInputErrorClass($input) {
    $input.classList.add(FORM_INPUT_INVALID_CLASS);
  }

  removeInputErrorClass($input) {
    $input.classList.remove(FORM_INPUT_INVALID_CLASS);
  }

  showErrorMessage($input, errorMessage) {
    const $errorMessageDiv = $input.nextElementSibling;

    $errorMessageDiv.innerText += ` ${errorMessage}`;
  }

  removeErrorMessage($input) {
    const $errorMessageDiv = $input.nextElementSibling;

    $errorMessageDiv.innerText = "";
  }
}
