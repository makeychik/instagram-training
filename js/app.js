import Router from "./router.js";
import RegistrationRoute from "./registration/registrationRoute.js";
import LoginRoute from "./login/loginRoute.js";

(function () {
  const router = new Router([
    new LoginRoute(),
    new RegistrationRoute(),
  ]);
})();
